﻿using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.AspNetCore.Components;

namespace BlazorApp4.Shared.Table
{
    public partial class Table<T> where T : INotifyPropertyChanged
    {
        [Parameter]
        public RenderFragment<TableSettings> TableHeader { get; set; }

        [Parameter]
        public RenderFragment<T> RowTemplate { get; set; }

        [Parameter]
        public IReadOnlyList<T> Items { get; set; }

        [Parameter]
        public TableSettings Settings { get; set; } = new TableSettings();
    }
}
