﻿using System;
using System.ComponentModel;
using Microsoft.AspNetCore.Components;

namespace BlazorApp4.Shared.Table
{
    public partial class ItemCell: ComponentBase, IDisposable
    {
        #region Parameters

        [Parameter]
        public string Column { get; set; }

        [Parameter]
        public string? Class { get; set; }

        [Parameter]
        public RenderFragment ChildContent { get; set; }

        /// <summary>
        /// Модель, при обновлении которой ячейка будет перерисованна
        /// </summary>
        [Parameter]
        public INotifyPropertyChanged Item { get; set; }

        #endregion

        #region Overrides of ComponentBase

        protected override void OnInitialized()
        {
            base.OnInitialized();

            Item.PropertyChanged += HandleItemChanged;
        }

        #endregion

        #region Private methods


        private void HandleItemChanged(object sender,  PropertyChangedEventArgs args)
        {
            if (Column == args.PropertyName /* && !Hidden */)
            {
                InvokeAsync(StateHasChanged);
            }
        }

        #endregion

        #region Implementation of IDisposable

        public void Dispose()
        {
            Item.PropertyChanged -= HandleItemChanged;
        }

        #endregion
    }
}
