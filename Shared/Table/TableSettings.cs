﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;

namespace BlazorApp4.Shared.Table
{
    public class TableSettings
    {
        public Dictionary<string, bool> Columns { get; set; } = new Dictionary<string, bool>();

        public event EventHandler OnChanged;

        public void InvokeOnChanged()
        {
            OnChanged?.Invoke(this, EventArgs.Empty);
        }

        public List<ColumnDefinition> Cols;
    }

    public class ColumnDefinition
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public RenderFragment<ColumnDefinition> Render { get; set; } = DefaultRender;

        public ColumnDefinition(string id, string title)
        {
            Id = id;
            Title = title;
        }

        public ColumnDefinition(string id, string title, Type contentComponent)
        {
            Id = id;
            Title = title;

            if (!typeof(ComponentBase).IsAssignableFrom(contentComponent))
            {
                throw new ArgumentException($"{contentComponent.FullName} must be a Blazor Component");
            }

            Render = (column) => new RenderFragment(x => 
            { 
                x.OpenComponent(1, contentComponent); 
                x.CloseComponent(); 
            });
        }
        
        private static RenderFragment<ColumnDefinition> DefaultRender = (column) => (builder) =>
        {
            builder.OpenElement(1, "span");
            builder.AddContent(1, column.Title);
            builder.CloseElement();
        };
    }
}
