﻿using System.ComponentModel;

namespace BlazorApp4.Model
{
    public class OrderViewModel : INotifyPropertyChanged
    {
        public int Id { get; set; }

        public string Account { get; set; }

        public double Price { get; set; }

        public int Count { get; set; }

        public OrderViewModel(int id, string account, double price, int count)
        {
            Id = id;
            Account = account;
            Price = price;
            Count = count;
        }

        #region Implementation of INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void InvokeOnChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
